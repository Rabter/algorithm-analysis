#ifndef MATRIX_MATH_H
#define MATRIX_MATH_H

#include "matrix.h"

bool vinograd(Matrix &result, const Matrix &first, const Matrix &second);
bool vinograd_multithread(Matrix &result, const Matrix &first, const Matrix &second, unsigned threads_count);

#endif // MATRIX_MATH_H
