#include <thread>
#include "matrix_math.h"

bool vinograd(Matrix &result, const Matrix &first, const Matrix &second)
{
    if (!(first.is_valid() && second.is_valid() && (first.width() == second.height())))
        return false;
    unsigned common_size = first.width();
    unsigned resh = first.height(), resw = second.width();
    result.reset_matrix(resh, resw);

    int *row_factor = new int[resh];
    int *column_factor = new int[resw];

    unsigned i = 0, j;
    for (; i < resh; ++i)
    {
      row_factor[i] = 0;
      for (j = 1; j < common_size; j += 2)
          row_factor[i] -= first[i][j - 1] * first[i][j];
    }
    for (i = 0; i < resw; ++i)
    {
        column_factor[i] = 0;
        for (j = 1; j < common_size; j += 2)
            column_factor[i] -= second[j - 1][i] * second[j][i];
    }

    for (i = 0; i < resh; ++i)
    {
        for (j = 0; j < resw; ++j)
        {
            result[i][j] = row_factor[i] + column_factor[j];
            for (unsigned k = 1; k < common_size; k += 2)
                result[i][j] += (first[i][k] + second[k - 1][j]) * (first[i][k - 1] + second[k][j]);
        }
    }

    if (common_size % 2)
    {
        for (i = 0; i < resh; ++i)
        {
            for (j = 0; j < resw; ++j)
                result[i][j] += first[i][common_size - 1] * second[common_size - 1][j];
        }
    }
    return true;
}


void calc_row_factor(int *row_factor, int istart, unsigned rown, unsigned common_size, const Matrix &first)
{
    unsigned j;
    rown += istart;
    for (unsigned i = istart; i < rown; ++i)
    {
      row_factor[i] = 0;
      for (j = 1; j < common_size; j += 2)
          row_factor[i] -= first[i][j - 1] * first[i][j];
    }
}

void calc_col_factor(int *column_factor, int istart, unsigned coln, unsigned common_size, const Matrix &second)
{
    unsigned j;
    coln += istart;
    for (unsigned i = istart; i < coln; ++i)
    {
        column_factor[i] = 0;
        for (j = 1; j < common_size; j += 2)
            column_factor[i] -= second[j - 1][i] * second[j][i];
    }
}

void multiply(Matrix &result, unsigned row_start, unsigned row_stop, unsigned col_start, unsigned col_stop, unsigned common_size, int *row_factor, int *column_factor, const Matrix &first, const Matrix &second)
{
    unsigned j;
    for (unsigned i = row_start; i < row_stop; ++i)
    {
        for (j = col_start; j < col_stop; ++j)
        {
            result[i][j] = row_factor[i] + column_factor[j];
            for (unsigned k = 1; k < common_size; k += 2)
                result[i][j] += (first[i][k] + second[k - 1][j]) * (first[i][k - 1] + second[k][j]);
        }
    }
}

void add_uneven(Matrix &result, unsigned row_start, unsigned row_stop, unsigned col_start, unsigned col_stop, unsigned common_size, const Matrix &first, const Matrix &second)
{
    unsigned j;
    for (unsigned i = row_start; i < row_stop; ++i)
    {
        for (j = col_start; j < col_stop; ++j)
            result[i][j] += first[i][common_size - 1] * second[common_size - 1][j];
    }
}

bool vinograd_multithread(Matrix &result, const Matrix &first, const Matrix &second, unsigned threads_count)
{
    if (!(first.is_valid() && second.is_valid() && (first.width() == second.height())) || !threads_count)
        return false;
    unsigned common_size = first.width();
    unsigned resh = first.height(), resw = second.width();
    result.reset_matrix(resh, resw);
    int *row_factor = new int[resh];
    int *column_factor = new int[resw];
    std::thread **threads = new std::thread*[threads_count];
    bool remainder = resh % threads_count;
    unsigned i, j, k = 0, step = resh / threads_count + remainder;
    for (i = 0, j = step; j <= resh; i = j, j += step)
        threads[k++] = new std::thread(calc_row_factor, row_factor, i, step, common_size, std::cref(first));
    if (remainder)
        threads[k++] = new std::thread(calc_row_factor, row_factor, i, resh - i, common_size, std::cref(first));
    for (i = 0; i < k; ++i)  // Joining from zero to k should be faster than joining from k to 0 because older threads are more likely to finish earlier
        threads[i]->join();
    k = 0;

    remainder = resw % threads_count;
    step = resw / threads_count + remainder;
    for (i = 0, j = step; j <= resw; i = j, j += step)
        threads[k++] = new std::thread(calc_col_factor, column_factor, i, step, common_size, std::cref(second));
    if (remainder)
        threads[k++] = new std::thread(calc_col_factor, column_factor, i, resw - i, common_size, std::cref(second));
    for (i = 0; i < k; ++i)
        threads[i]->join();
    k = 0;

    unsigned tmp = threads_count;
    unsigned horizontal_divs = 1, vertical_divs = 1;
    while (tmp != 1)
    {
        vertical_divs <<= 1;
        tmp >>= 1;
        if (tmp != 1)
        {
            horizontal_divs <<= 1;
            tmp >>= 1;
        }
    }

    bool remainder_v = resh % vertical_divs;
    remainder  = resw % horizontal_divs;
    unsigned vstep = resh  / vertical_divs + remainder_v;
    step = resw / horizontal_divs + bool(remainder);
    unsigned vi, vj;
    for (vi = 0, vj = vstep; vj <= resh; vi = vj, vj += vstep)
    {
        for (i = 0, j = step; j <= resw; i = j, j += step)
            threads[k++] = new std::thread(multiply, std::ref(result), vi, vj, i, j, common_size, row_factor, column_factor, std::cref(first), std::cref(second));
        if (remainder)
            threads[k++] = new std::thread(multiply, std::ref(result), vi, vj, i, resw, common_size, row_factor, column_factor, std::cref(first), std::cref(second));
    }
    if (remainder_v)
    {
        for (i = 0, j = step; j <= resw; i = j, j += step)
            threads[k++] = new std::thread(multiply, std::ref(result), vi, resh, i, j, common_size, row_factor, column_factor, std::cref(first), std::cref(second));
        if (remainder)
            threads[k++] = new std::thread(multiply, std::ref(result), vi, resh, i, resw, common_size, row_factor, column_factor, std::cref(first), std::cref(second));
    }
    for (i = 0; i < k; ++i)
        threads[i]->join();
    k = 0;

    if (common_size % 2)
    {
        for (unsigned vi = 0, vj = vstep; vj <= resh; vi = vj, vj += vstep)
        {
            for (i = 0, j = step; j <= resw; i = j, j += step)
                threads[k++] = new std::thread(add_uneven, std::ref(result), vi, vj, i, j, common_size, std::cref(first), std::cref(second));
            if (remainder)
                threads[k++] = new std::thread(add_uneven, std::ref(result), vi, vj, i, j, common_size, std::cref(first), std::cref(second));
        }
        if (remainder_v)
        {
            for (i = 0, j = step; j <= resw; i = j, j += step)
                threads[k++] = new std::thread(add_uneven, std::ref(result), vi, resh, i, j, common_size, std::cref(first), std::cref(second));
            if (remainder)
                threads[k++] = new std::thread(add_uneven, std::ref(result), vi, resh, i, j, common_size, std::cref(first), std::cref(second));
        }
        for (i = 0; i < k; ++i)
            threads[i]->join();
        k = 0;
    }

    return true;
}
