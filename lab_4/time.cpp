#include "building_mode.h"
#ifdef TIMING_MODE

#include <chrono>
#include <fstream>
#include <iostream>
#include "matrix_math.h"
#define LOGICAL_PROCESSORS_AMOUNT 4
#define MAX_THREADS_COUNT 64
#define TIMERS_AMOUNT 2 * (LOGICAL_PROCESSORS_AMOUNT + 3)

int main()
{
    std::ofstream fout("time_results.csv");
    fout << ";;With N threads\n";
    fout << "Size;Threadless";
    for (unsigned i = 1; i <= MAX_THREADS_COUNT; i <<= 1)
        fout << ";N = " << i;
    fout << '\n';
    Matrix first, second, result;
    std::chrono::time_point<std::chrono::system_clock> t1, t2;
    std::chrono::time_point<std::chrono::system_clock> tmp[TIMERS_AMOUNT];
    for (unsigned size = 100, divider = 1; size <= 1000; size += 100, ++divider)
    {
        std::cout << "Timetesting on " << size << 'x' << size << "...\n";
        long double t[LOGICAL_PROCESSORS_AMOUNT + 4] = { 0 };
        first.reset_matrix(size, size);
        second.reset_matrix(size, size);
        first.fill();
        second.fill();

        unsigned limit = 10 / divider;
        for (unsigned repeat_num = 0; repeat_num < limit; ++repeat_num)
        {
            t1 = std::chrono::system_clock::now();
            vinograd(result, first, second);
            t2 = std::chrono::system_clock::now();
            unsigned i = 0, j = 1;
            for (unsigned threads = 1; threads <= MAX_THREADS_COUNT; threads <<= 1)
            {
                tmp[i] = std::chrono::system_clock::now();
                vinograd_multithread(result, first, second, threads);
                tmp[j] = std::chrono::system_clock::now();
                i += 2;
                j += 2;
            }
            t[0] += static_cast<long double>(std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
            for (unsigned index = 1, i = 1; i < TIMERS_AMOUNT; ++index, i += 2)
                t[index] += static_cast<long double>(std::chrono::duration_cast<std::chrono::nanoseconds>(tmp[i] - tmp[i - 1]).count());
        }
        fout << size;
        for (unsigned i = 0; i < LOGICAL_PROCESSORS_AMOUNT + 4; ++i)
            fout << ';' << t[i] / limit;
        fout << '\n';
    }
    fout << ";;;\n";
    for (unsigned size = 101, divider = 1; size <= 1001; size += 100, ++divider)
    {
        std::cout << "Timetesting on " << size << 'x' << size << "...\n";
        long double t[LOGICAL_PROCESSORS_AMOUNT + 4] = { 0 };
        first.reset_matrix(size, size);
        second.reset_matrix(size, size);
        first.fill();
        second.fill();

        unsigned limit = 10 / divider;
        for (unsigned repeat_num = 0; repeat_num < limit; ++repeat_num)
        {
            t1 = std::chrono::system_clock::now();
            vinograd(result, first, second);
            t2 = std::chrono::system_clock::now();
            unsigned i = 0, j = 1;
            for (unsigned threads = 1; threads <= MAX_THREADS_COUNT; threads <<= 1)
            {
                tmp[i] = std::chrono::system_clock::now();
                vinograd_multithread(result, first, second, threads);
                tmp[j] = std::chrono::system_clock::now();
                i += 2;
                j += 2;
            }
            t[0] += static_cast<long double>(std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
            for (unsigned index = 1, i = 1; i < TIMERS_AMOUNT; ++index, i += 2)
                t[index] += static_cast<long double>(std::chrono::duration_cast<std::chrono::nanoseconds>(tmp[i] - tmp[i - 1]).count());
        }
        fout << size;
        for (unsigned i = 0; i < LOGICAL_PROCESSORS_AMOUNT + 4; ++i)
            fout << ';' << t[i] / limit;
        fout << '\n';
    }
    std::cout <<"Finished timetesting\n";
}

#endif
