#include "building_mode.h"
#ifdef REGULAR_MODE
#include <iostream>
#include "matrix_math.h"

unsigned long long tick()
{
    unsigned long long d;
    __asm__ __volatile__ ("rdtsc" : "=A" (d) );
    return d;
}

int main()
{
    unsigned n, m;
    std::cout << "Enter height and width of the first matrix:\n";
    std::cin >> n >> m;
    if (n == 0 || m == 0)
    {
        std::cout << "Wrong input size\n";
        return 1;
    }
    Matrix first(n, m);
    std::cout << "Enter height and width of the second matrix:\n";
    std::cin >> n >> m;
    if (n == 0 || m == 0)
    {
        std::cout << "Wrong input size\n";
        return 1;
    }
    Matrix second(n, m);

    if (first.width() != second.height())
        std::cout << "Wrong input size\n";
    else
    {
        int choice;
        std::cout << "Type 1 for manual input or 0 for random values:\n";
        std::cin >> choice;
        if (choice)
        {
            std::cout << "Enter first matrix:\n";
            for (unsigned i = 0; i < first.height(); ++i)
                for (unsigned j = 0; j < first.width(); ++j)
                    std::cin >> first[i][j];
            std::cout << "Enter second matrix:\n";
            for (unsigned i = 0; i < second.height(); ++i)
                for (unsigned j = 0; j < second.width(); ++j)
                    std::cin >> second[i][j];
        }
        else
        {
            first.fill();
            second.fill();
        }

        Matrix result_threadless, result;
        unsigned long long threadless1, threadless2;
        unsigned long long t[14];

        threadless1 = tick();
        vinograd(result_threadless, first, second);
        threadless2 = tick();

        unsigned i = 0, j = 1;
        for (unsigned threads = 1; threads <= 64; threads <<= 1)
        {
            t[i] = tick();
            vinograd_multithread(result, first, second, threads);
            t[j] = tick();
            i += 2;
            j += 2;
            if (!(result == result_threadless))
                std::cout << "Incorrect answer at " << threads << " threads\n";
        }

        std::cout << "\nResult:\n" << result_threadless << std::endl;
        std::cout << "\nMultiplication time:\n";
        std::cout << "Threadless: " << threadless2 - threadless1 << " ticks" << std::endl;
        for (i = j = 1; i < 14; i += 2, j <<= 1)
            std::cout << j << "  threads: " << t[i] - t[i - 1] << " ticks" << std::endl;
    }
}

#endif
