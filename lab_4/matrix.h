#ifndef MATRIX_H
#define MATRIX_H
#include <iostream>

class Matrix
{
public:
    Matrix();
    Matrix(unsigned height, unsigned width);
    Matrix(Matrix &&other);
    ~Matrix();

    inline unsigned height() const
    { return n; }
    inline unsigned width() const
    { return m; }

    void reset_height(unsigned new_height);
    void reset_width(unsigned new_width);
    void reset_matrix(unsigned new_heigth, unsigned new_width);

    void fill();
    void clear();

    bool is_valid() const;

    inline int* operator [] (unsigned index)
    { return mat[index]; }
    inline const int* operator [] (unsigned index) const
    { return mat[index]; }

    Matrix& operator = (Matrix &&other);

    friend std::ostream& operator << (std::ostream &stream, const Matrix &mat);
    friend bool operator == (const Matrix &a, const Matrix &b);

protected:
    unsigned n, m;
    int **mat;
};

#endif // MATRIX_H
