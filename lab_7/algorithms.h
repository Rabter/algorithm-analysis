#ifndef ALGORITHMS_H
#define ALGORITHMS_H

#include <string>

int standart(std::string str, std::string substr);
int kmp(std::string str, std::string substr);
int bm(std::string str, std::string substr);

#endif // ALGORITHMS_H
