#include "building_mode.h"
#ifdef REGULAR_MODE

#include <iostream>
#include "algorithms.h"

int main()
{
    std::string str, substr;
    std::cout << "Enter main string:\n";
    std::getline(std::cin, str);
    std::cout << "Enter substring to search for:\n";
    std::getline(std::cin, substr);
    std::cout << "Standart algorithm result:\n" << standart(str, substr) + 1;
    std::cout << "\nKMP algorithm result:\n" << kmp(str, substr) + 1;
    std::cout << "\nBM algorithm result:\n" << bm(str, substr) + 1;
    return 0;
}

#endif
