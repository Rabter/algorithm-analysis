#include <vector>
#include <map>
#include "algorithms.h"

int standart(std::string str, std::string substr)
{
    int str_len = str.length();
    int sub_len = substr.length();

    if (str_len == 0 || sub_len == 0)
        return -1;

    for (int i = 0; i <= str_len - sub_len; ++i)
    {
        int tmp_i = i;
        for (int j = 0; substr[j] == str[tmp_i]; ++j)
        {
            if (j == sub_len-1)
                return i;
            ++tmp_i;
        }
    }

    return -1;
}

void create_prefix(std::vector<unsigned> &prefix, const std::string &str, unsigned size)
{
    prefix.resize(size);
    prefix[0] = 0;

    for (unsigned i = 1; i < size; ++i)
    {
        unsigned j = prefix[i - 1];

        while (j && str[i] != str[j])
            j = prefix[j - 1];

        if (str[i] == str[j])
            ++j;

        prefix[i] = j;
    }

}

int kmp(std::string str, std::string substr)
{
    unsigned str_len = str.length();
    unsigned sub_len = substr.length();

    if (str_len < sub_len || str_len == 0 || sub_len == 0)
        return -1;

    std::vector<unsigned> prefixes;
    create_prefix(prefixes, substr, sub_len);


    for (unsigned j = 0, i = 0; i < str_len; ++i)
    {

        while (j && str[i] != substr[j])
            j = prefixes[j - 1];

        if (str[i] == substr[j])
            j++;

        if (j == sub_len)
            return i + 1 - j;
    }
    return -1;
}

void create_shift(std::map<char, unsigned> &shift, const std::string &substr)
{
    auto sub_size = substr.length();

    for (unsigned symb = 0; symb < sub_size - 1; ++symb)
        shift[substr[symb]] = sub_size - symb - 1;
}

unsigned jump(const std::map<char, unsigned> &shift, char key, unsigned total_size)
{
    /*std::map<char, unsigned>::const_iterator*/ auto pos = shift.find(key);
    if (pos == shift.cend())
        return total_size;
    return pos->second;
}

int bm(std::string str, std::string substr)
{
    auto str_len = str.length();
    auto sub_len = substr.length();

    if (str_len < sub_len || str_len == 0 || sub_len == 0)
        return -1;

    std::map<char, unsigned> shift;

    create_shift(shift, substr);

    int start = sub_len - 1;
    int i = start, j = start, k = start;

    while (j >= 0 && i < int(str_len))
    {
        j = start;
        k = i;

        while (j >= 0 && str[k] == substr[j])
        {
            --k;
            --j;
        }

        i += jump(shift, str[i], sub_len);
    }
    return k < int(str_len - sub_len) ? k + 1 : -1;
}

