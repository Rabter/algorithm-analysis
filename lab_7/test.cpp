#include "building_mode.h"
#ifdef TEST_MODE

#include <iostream>
#include <string>
#include "algorithms.h"

void test_standart()
{
    printf("Testing 'standart()':\n");
    int testnum = 1;
    {
        std::string str = "", substr = "";
        int expected = -1, res = standart(str, substr);
        if(res == expected)
            std::cout << "OK" << std::endl;
        else
            std::cout << "WRONG" << std::endl;
        ++testnum;
    }
    {
        std::string str = "", substr = "aaa";
        int expected = -1, res = standart(str, substr);
        if(res == expected)
            std::cout << "OK" << std::endl;
        else
            std::cout << "WRONG" << std::endl;
        ++testnum;
    }
    {
        std::string str = "aaa", substr = "";
        int expected = -1, res = standart(str, substr);
        if(res == expected)
            std::cout << "OK" << std::endl;
        else
            std::cout << "WRONG" << std::endl;
        ++testnum;
    }
    {
        std::string str = "a", substr = "a";
        int expected = 0, res = standart(str, substr);
        if(res == expected)
            std::cout << "OK" << std::endl;
        else
            std::cout << "WRONG" << std::endl;
        ++testnum;
    }
    {
        std::string str = "there they are", substr = "they";
        int expected = 6, res = standart(str, substr);
        if(res == expected)
            std::cout << "OK" << std::endl;
        else
            std::cout << "WRONG" << std::endl;
        ++testnum;
    }
    {
        std::string str = "ababcd", substr = "abab";
        int expected = 0, res = standart(str, substr);
        if(res == expected)
            std::cout << "OK" << std::endl;
        else
            std::cout << "WRONG" << std::endl;
        ++testnum;
    }
    {
        std::string str = "ababcd", substr = "abcd";
        int expected = 2, res = standart(str, substr);
        if(res == expected)
            std::cout << "OK" << std::endl;
        else
            std::cout << "WRONG" << std::endl;
        ++testnum;
    }
}

void test_kmp()
{
    printf("Testing 'kmp()':\n");
    int testnum = 1;
    {
        std::string str = "", substr = "";
        int expected = -1, res = kmp(str, substr);
        if(res == expected)
            std::cout << "OK" << std::endl;
        else
            std::cout << "WRONG" << std::endl;
        ++testnum;
    }
    {
        std::string str = "", substr = "aaa";
        int expected = -1, res = kmp(str, substr);
        if(res == expected)
            std::cout << "OK" << std::endl;
        else
            std::cout << "WRONG" << std::endl;
        ++testnum;
    }
    {
        std::string str = "aaa", substr = "";
        int expected = -1, res = kmp(str, substr);
        if(res == expected)
            std::cout << "OK" << std::endl;
        else
            std::cout << "WRONG" << std::endl;
        ++testnum;
    }
    {
        std::string str = "a", substr = "a";
        int expected = 0, res = kmp(str, substr);
        if(res == expected)
            std::cout << "OK" << std::endl;
        else
            std::cout << "WRONG" << std::endl;
        ++testnum;
    }
    {
        std::string str = "there they are", substr = "they";
        int expected = 6, res = kmp(str, substr);
        if(res == expected)
            std::cout << "OK" << std::endl;
        else
            std::cout << "WRONG" << std::endl;
        ++testnum;
    }
    {
        std::string str = "ababcd", substr = "abab";
        int expected = 0, res = kmp(str, substr);
        if(res == expected)
            std::cout << "OK" << std::endl;
        else
            std::cout << "WRONG" << std::endl;
        ++testnum;
    }
    {
        std::string str = "ababcd", substr = "abcd";
        int expected = 2, res = kmp(str, substr);
        if(res == expected)
            std::cout << "OK" << std::endl;
        else
            std::cout << "WRONG" << std::endl;
        ++testnum;
    }
}

void test_bm()
{
    printf("Testing 'bm()':\n");
    int testnum = 1;
    {
        std::string str = "", substr = "";
        int expected = -1, res = bm(str, substr);
        if(res == expected)
            std::cout << "OK" << std::endl;
        else
            std::cout << "WRONG" << std::endl;
        ++testnum;
    }
    {
        std::string str = "", substr = "aaa";
        int expected = -1, res = bm(str, substr);
        if(res == expected)
            std::cout << "OK" << std::endl;
        else
            std::cout << "WRONG" << std::endl;
        ++testnum;
    }
    {
        std::string str = "aaa", substr = "";
        int expected = -1, res = bm(str, substr);
        if(res == expected)
            std::cout << "OK" << std::endl;
        else
            std::cout << "WRONG" << std::endl;
        ++testnum;
    }
    {
        std::string str = "a", substr = "a";
        int expected = 0, res = bm(str, substr);
        if(res == expected)
            std::cout << "OK" << std::endl;
        else
            std::cout << "WRONG" << std::endl;
        ++testnum;
    }
    {
        std::string str = "there they are", substr = "they";
        int expected = 6, res = bm(str, substr);
        if(res == expected)
            std::cout << "OK" << std::endl;
        else
            std::cout << "WRONG" << std::endl;
        ++testnum;
    }
    {
        std::string str = "ababcd", substr = "abab";
        int expected = 0, res = bm(str, substr);
        if(res == expected)
            std::cout << "OK" << std::endl;
        else
            std::cout << "WRONG" << std::endl;
        ++testnum;
    }
    {
        std::string str = "ababcd", substr = "abcd";
        int expected = 2, res = bm(str, substr);
        if(res == expected)
            std::cout << "OK" << std::endl;
        else
            std::cout << "WRONG" << std::endl;
        ++testnum;
    }
}

int main()
{
    test_standart();
    test_kmp();
    test_bm();
}

#endif
