#ifndef BRUTE_FORCE_H
#define BRUTE_FORCE_H

#include <vector>
#include "matrix.h"

void brute_force(std::vector<unsigned> &min_path, const Matrix<unsigned> &graph);

#endif // BRUTE_FORCE_H
