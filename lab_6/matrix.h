#ifndef MATRIX_H
#define MATRIX_H
#include <iostream>

template <class T>
class Matrix
{
public:
    Matrix();
    Matrix(unsigned height, unsigned width);
    Matrix(Matrix &&other);
    ~Matrix();

    inline unsigned height() const
    { return n; }
    inline unsigned width() const
    { return m; }

    void reset_height(unsigned new_height);
    void reset_width(unsigned new_width);
    void reset_matrix(unsigned new_heigth, unsigned new_width);

    void fill();
    void clear();

    bool is_valid() const;

    inline T* operator [] (unsigned index)
    { return mat[index]; }
    inline const T* operator [] (unsigned index) const
    { return mat[index]; }

    Matrix<T> &operator =(Matrix<T> &&other);

    template <class C>
    friend std::ostream& operator << (std::ostream &stream, const Matrix<C> &mat);
    template <class C>
    friend bool operator == (const Matrix<C> &a, const Matrix<C> &b);

protected:
    unsigned n, m;
    T **mat;
};

#include <matrix_impl.h>

#endif // MATRIX_H
