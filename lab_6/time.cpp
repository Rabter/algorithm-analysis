#include "building_mode.h"
#ifdef TIMING_MODE

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <chrono>
#include "brute_force.h"
#include "matrix.h"
#include "ants.h"
#define CITIES_COUNT 10

unsigned path_length(const Matrix<unsigned> &graph, const std::vector<unsigned> &path)
{
    unsigned length = graph[path[path.size() - 1]][path[0]];
    for (unsigned i = 0; i < path.size() - 1; ++i)
        length += graph[path[i]][path[i + 1]];
    return length;
}

void fill_matrix(Matrix<unsigned> &cities, unsigned size_count)
{
    for (unsigned i = 0; i < size_count; ++i)
        for (unsigned j = 0; j < size_count; ++j)
        {
            if (i == j)
                cities[i][j] = 0;
            else
                cities[i][j] = rand() + 1;
        }
}

int main()
{
    srand(time(NULL));
    std::ofstream fout("parametrization.csv");
    fout << "tmax;evaporation;herd;devitation" << std::endl;
    Matrix<unsigned> cities(CITIES_COUNT,CITIES_COUNT);
    std::vector<unsigned> accurate, euristic;
    std::vector<double> evaporation = { 0, 0.25, 0.5, 0.75, 1 }, herd = { 0, 0.25, 0.5, 0.75, 1 };
    std::vector<unsigned> tmax(0);
    for (unsigned i = 280; i < 310; i += 10)
        tmax.push_back(i);
    for (unsigned tm: tmax)
    {
        for (double vape: evaporation)
        {
            for (double h: herd)
            {
                unsigned p1 = 0, p2 = 0;
                for (unsigned i = 0; i < 10; ++i)
                {
                    fill_matrix(cities, CITIES_COUNT);
                    brute_force(accurate, cities);
                    ants(euristic, cities, tm, h, 1 - h, vape);
                    p1 += path_length(cities, accurate);
                    p2 += path_length(cities, euristic);
                }
                fout << tm << ';' << vape << ';' << h << ';' << double(p2 - p1) / p1 << std::endl;
            }
        }
    }
    std::cout << "Finished parametrization\n";
    fout.close();
    fout.open("time_results.csv");
    fout << "Size;Brute force;Ants" << std::endl;
    std::chrono::time_point<std::chrono::system_clock> t1, t2, t3;
    for (unsigned size = 2; size < 17; ++size)
    {
        double s1 = 0, s2 = 0;
        for (unsigned i = 0; i < 10; ++i)
        {
            cities.reset_matrix(size, size);
            fill_matrix(cities, size);
            t1 = std::chrono::system_clock::now();
            brute_force(accurate, cities);
            t2 = std::chrono::system_clock::now();
            ants(euristic, cities, 290, 0.25, 0.75, 0.5);
            t3 = std::chrono::system_clock::now();
            s1 += static_cast<long double>(std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
            s2 += static_cast<long double>(std::chrono::duration_cast<std::chrono::nanoseconds>(t3 - t2).count());
        }
        fout << size << ';' << s1 / 10 << ';' << s2 / 10 << std::endl;
    }
    std::cout << "Finished timetesting\n";
    fout.close();
}
#endif
