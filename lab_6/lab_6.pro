TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        ants.cpp \
        main.cpp \
    time.cpp \
    brute_force.cpp

HEADERS += \
    ants.h \
    matrix.h \
    matrix_impl.h \
    building_mode.h \
    brute_force.h
