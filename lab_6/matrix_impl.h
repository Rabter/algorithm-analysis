#include <cstdlib>
#include <ctime>
#include "matrix.h"

template <class T>
Matrix<T>::Matrix(): n(0), m(0), mat(nullptr) {}

template <class T>
Matrix<T>::Matrix(unsigned height, unsigned width): n(height), m(width)
{
    mat = new T*[n];
    for (unsigned i = 0; i < n; ++i)
        mat[i] = new T[m];
    srand(time(NULL));
}

template <class T>
Matrix<T>::Matrix(Matrix &&other)
{
    mat = other.mat;
    n = other.n;
    m = other.m;
    other.mat = nullptr;
}

template <class T>
Matrix<T>::~Matrix()
{
    if (mat)
    {
        for (unsigned i = 0; i < n; ++i)
            delete mat[i];
        delete mat;
    }
}

template <class T>
void Matrix<T>::reset_height(unsigned new_height)
{
    T **new_mat = new T*[new_height];
    unsigned minh = (new_height < n? new_height : n), i = 0;
    for (;i < minh; ++i)
        new_mat[i] = mat[i];
    if (new_height < n)
    {
        for (;i < new_height; ++i)
            delete mat[i];
    }
    else
    {
        for (;i < new_height; ++i)
            new_mat[i] = new T[m];
    }
    delete mat;
    mat = new_mat;
    n = new_height;
}

template <class T>
void Matrix<T>::reset_width(unsigned new_width)
{
    for (unsigned i = 0; i < n; ++i)
    {
        T *new_row = new T[new_width];
        unsigned minw = (new_width < m? new_width : m);
        for (unsigned j = 0; j < minw; ++j)
            new_row[j] = mat[i][j];
        delete mat[i];
        mat[i] = new_row;
    }
    m = new_width;
}

template <class T>
void Matrix<T>::reset_matrix(unsigned new_heigth, unsigned new_width)
{
    if (mat)
    {
        for (unsigned i = 0; i < n; ++i)
            delete mat[i];
        delete mat;
    }
    n = new_heigth;
    m = new_width;
    *this = Matrix<T>(new_heigth, new_width);
}

template <class T>
void Matrix<T>::fill()
{
    if (is_valid())
        for (unsigned i = 0; i < n; ++i)
            for (unsigned j = 0; j < m; ++j)
                mat[i][j] = T(rand());
}

template <class T>
void Matrix<T>::clear()
{
    if (mat)
    {
        for (unsigned i = 0; i < n; ++i)
            delete mat[i];
        delete mat;
        mat = nullptr;
    }
    n = m = 0;
}

template <class T>
bool Matrix<T>::is_valid() const
{
    return mat;
}

template <class T>
Matrix<T>& Matrix<T>::operator =(Matrix<T> &&other)
{
    mat = other.mat;
    n = other.n;
    m = other.m;
    other.mat = nullptr;
    return *this;
}

template <class C>
bool operator ==(const Matrix<C> &a, const Matrix<C> &b)
{
    if (a.height() != b.height() || a.width() != b.width())
        return false;
    for (unsigned i = 0; i < a.height(); ++i)
        for (unsigned j = 0; j < a.width(); ++j)
            if (a[i][j] != b[i][j])
                return false;
    return true;
}

template <class C>
std::ostream& operator <<(std::ostream &stream, const Matrix<C> &mat)
{
    for (unsigned i = 0; i < mat.n; ++i)
    {
        for (unsigned j = 0; j < mat.m; ++j)
            stream << mat[i][j] << ' ';
        stream << '\n';
    }
    return stream;
}
