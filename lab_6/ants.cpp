#include <cmath>
#include <cstdlib>
#include "ants.h"
#define PHER_MIN 1
#define ANT_ODOUR 1.0

double chance()
{
    return rand() % 10000 / 10000.0;
}

void ants(std::vector<unsigned> &min_path, const Matrix<unsigned> &graph, unsigned tmax, double herd, double greed, double evaporation)
{
    min_path = {};
    if (graph.width() != graph.height())
        return;
    unsigned cities_count = graph.width();
    for (unsigned i = 0; i < cities_count; ++i)
        for (unsigned j = 0; j < cities_count; ++j)
            if (i != j && graph[i][j] <= 0)
                return;

    Matrix<double> pheromones(cities_count, cities_count);
    Matrix<double> delta_pheromones(cities_count, cities_count);
    for (unsigned i = 0; i < cities_count; ++i)
        for (unsigned j = 0; j < cities_count; ++j)
            pheromones[i][j] = PHER_MIN;

    unsigned min_path_length = 0;
    for (unsigned day = 0; day < tmax; ++day)
    {
        for (unsigned i = 0; i < cities_count; ++i)
            for (unsigned j = 0; j < cities_count; ++j)
                delta_pheromones[i][j] = 0;
        for (unsigned ant = 0; ant < cities_count; ++ant)
        {
            std::vector<unsigned> not_visited(0);
            unsigned current_city = ant;
            std::vector<unsigned> path = {current_city};
            unsigned path_length = 0;
            for (unsigned i = 0; i < cities_count; ++i)
                if (i != current_city)
                    not_visited.push_back(i);
            unsigned size = not_visited.size();
            while (size)
            {
                std::vector<double> p(size, 0);
                for (unsigned i = 0; i < size; ++i)
                {
                    double sum = 0;
                    for (unsigned j = 0; j < size; ++j)
                        sum += pow(pheromones[current_city][not_visited[j]], herd) / pow(graph[current_city][not_visited[j]], greed);
                    p[i] = pow(pheromones[current_city][not_visited[i]], herd) / pow(graph[current_city][not_visited[i]], greed) / sum;
                }

                double destiny = chance();
                double sum = 0;
                unsigned i = -1;
                do
                    sum += p[++i];
                while (sum < destiny);

                path_length += graph[current_city][not_visited[i]];
                delta_pheromones[current_city][not_visited[i]] += ANT_ODOUR / path_length;
                current_city = not_visited[i];
                path.push_back(current_city);
                not_visited.erase(not_visited.begin() + i);
                --size;
            }

            if (((day == 0) && (ant == 0)) || (path_length < min_path_length))
            {
                min_path_length = path_length;
                min_path = path;
            }

            for (unsigned i = 0; i < cities_count; ++i)
                for (unsigned j = 0; j < cities_count; ++j)
                {
                    double new_value = pheromones[i][j] * (1 - evaporation) + delta_pheromones[i][j];
                    pheromones[i][j] = (new_value < PHER_MIN? PHER_MIN : new_value);
                }
        }
    }
}
