#ifndef ANTS_H
#define ANTS_H

#include <vector>
#include "matrix.h"

void ants(std::vector<unsigned> &path, const Matrix<unsigned> &graph, unsigned tmax, double herd, double greed, double evaporation);

#endif // ANTS_H
