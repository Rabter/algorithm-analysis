#include "building_mode.h"
#ifdef REGULAR_MODE
#include <iostream>
#include <vector>
#include "ants.h"
#include "brute_force.h"

unsigned path_length(const Matrix<unsigned> &graph, const std::vector<unsigned> &path)
{
    unsigned length = graph[path[path.size() - 1]][path[0]];
    for (unsigned i = 0; i < path.size() - 1; ++i)
        length += graph[path[i]][path[i + 1]];
    return length;
}

void fill_matrix(Matrix<unsigned> &cities, unsigned cities_count)
{
    for (unsigned i = 0; i < cities_count; ++i)
        for (unsigned j = 0; j < cities_count; ++j)
        {
            if (i == j)
                cities[i][j] = 0;
            else
                cities[i][j] = rand() + 1;
        }
}

int main()
{
    unsigned n;
    std::cout << "Enter count of cities:\n";
    std::cin >> n;
    Matrix<unsigned> cities(n, n);
    srand(time(NULL));
    int choice;
    std::cout << "Type 1 for manual input or 0 for random values:\n";
    std::cin >> choice;
    if (choice)
    {
        std::cout << "Enter graph matrix:\n";
        for (unsigned i = 0; i < n; ++i)
            for (unsigned j = 0; j < n; ++j)
                std::cin >> cities[i][j];
    }
    else
        fill_matrix(cities, n);
    std::vector<unsigned> accurate, euristic;
    brute_force(accurate, cities);
    ants(euristic, cities, 290, 0.25, 0.75, 0.5);
    unsigned p1 = path_length(cities, accurate), p2 = path_length(cities, euristic);

    for (unsigned i: accurate)
        std::cout << i << ' ';
    std::cout << "shortest length: " << p1 << '\n';
    for (unsigned i: euristic)
        std::cout << i << ' ';
    std::cout << "eurisic length: " << p2 << '\n';
    std::cout << "Devitation: " << double(p2 - p1) / p1;

    return 0;
}

#endif
