#include "brute_force.h"

void hamilton(const Matrix<unsigned> &distances, std::vector<unsigned> &min_path, int &min_distance, std::vector<unsigned> &cur_path, std::vector<bool> &visited, int &cur_len)
{
   if (cur_path.size() == distances.width())
   {
       int tmp = distances[cur_path.back()][cur_path[0]];
       if (cur_len + tmp < min_distance)
       {
           min_path = cur_path;
           min_distance = cur_len + tmp;
       }
       return;
   }
   for (unsigned i = 0; i < distances.width(); i++)
   {
       if (!visited[i])
       {
           int tmp = distances[cur_path.back()][i];
           if (cur_len + tmp > min_distance)
               continue;
           cur_len += tmp;
           cur_path.push_back(i);
           visited[i] = 1;
           hamilton(distances, min_path, min_distance, cur_path, visited, cur_len);
           visited[i] = 0;
           cur_path.pop_back();
           cur_len -= tmp;
       }
   }
}

void brute_force(std::vector<unsigned> &min_path, const Matrix<unsigned> &graph)
{
   int n = graph.width();
   std::vector<bool> visited(n);
   std::vector<unsigned> cur_path;
   int cur_len = 0;
   int min_path_len = INT_MAX;
   for (int i = 0; i < n; i++)
   {
       cur_path.clear();
       cur_path.push_back(i);
       visited = {0};
       visited[i] = 1;
       cur_len = 0;
       hamilton(graph, min_path, min_path_len, cur_path, visited, cur_len);
   }
}
