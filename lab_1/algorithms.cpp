#include "algorithms.h"
#include "building_mode.h"
#ifdef USUAL_MODE
#include <iostream>
#endif

static inline min(unsigned a, unsigned b)
{
    return (a < b? a : b);
}

unsigned recursive_levenstein(const std::string &s1, const std::string &s2)
{
    if (s1 == "")
        return s2.length();
    if (s2 == "")
        return s1.length();
    const char adder = (s1.back() != s2.back());
    std::string s1_short = s1, s2_short = s2;
    s1_short.pop_back();
    s2_short.pop_back();
    return min(min(recursive_levenstein(s1, s2_short) + 1, recursive_levenstein(s1_short, s2) + 1), recursive_levenstein(s1_short, s2_short) + adder);
}

static unsigned recursive_damerau_body(const std::string &s1, const std::string &s2, bool switchable)
{
    if (s1 == "")
        return s2.length();
    if (s2 == "")
        return s1.length();
    const char adder = (s1.back() != s2.back());
    unsigned min_dist;
    std::string s1_short = s1, s2_short = s2;
    s1_short.pop_back();
    s2_short.pop_back();
    min_dist = min(min(recursive_damerau_body(s1, s2_short, true) + 1, recursive_damerau_body(s1_short, s2, true) + 1), recursive_damerau_body(s1_short, s2_short, true) + adder);
    if (switchable)
    {
        unsigned s1_length = s1.length();
        if (s1_length != 1) // As it is also not zero, that means >=2
        {
            std::string s1_swapped = s1;
            std::swap(s1_swapped[s1_length - 1], s1_swapped[s1_length - 2]);
            min_dist = min(min_dist, recursive_damerau_body(s1_swapped, s2, false) + 1);
        }
    }
    return min_dist;
}

unsigned recursive_damerau(const std::string &s1, const std::string &s2)
{   // First call is always with true and should not depend on user
    return recursive_damerau_body(s1, s2, true);
}

unsigned matrix_levenstein(const std::string &s1, const std::string &s2)
{
    unsigned n = s1.length() + 1, m = s2.length() + 1;
    unsigned matrix[n][m];
    unsigned maxlen = (n > m? n : m);
    for (unsigned i = 0; i < maxlen; ++i)
    {
        if (i < n)
            matrix[i][0] = i;
        if (i < m)
            matrix[0][i] = i;
    }
    for (unsigned i = 1; i < n; ++i)
        for (unsigned j = 1; j < m; ++j)
        {
            unsigned char idec = i - 1, jdec = j - 1;
            unsigned d1 = matrix[idec][jdec] + (s1[idec] != s2[jdec]);
            unsigned d2 = matrix[idec][j] + 1;
            unsigned d3 = matrix[i][jdec] + 1;
            matrix[i][j] = min(d1, min(d2, d3));
        }
#ifdef USUAL_MODE
    for (unsigned i = 0; i < n; ++i)
     {
        for (unsigned j = 0; j < m; ++j)
            std::cout << matrix[i][j] << ' ';
        std::cout << '\n';
    }
#endif
    return matrix[n - 1][m - 1];
}

unsigned matrix_damerau(const std::string &s1, const std::string &s2)
{
    unsigned n = s1.length() + 1, m = s2.length() + 1;
    unsigned matrix[n][m];
    unsigned maxlen = (n > m? n : m);
    for (unsigned i = 0; i < maxlen; ++i)
    {
        if (i < n)
            matrix[i][0] = i;
        if (i < m)
            matrix[0][i] = i;
    }
    for (unsigned i = 1; i < n; ++i)
        for (unsigned j = 1; j < m; ++j)
        {
            unsigned char idec = i - 1, jdec = j - 1;
            unsigned d1 = matrix[idec][jdec] + (s1[idec] != s2[jdec]);
            unsigned d2 = matrix[idec][j] + 1;
            unsigned d3 = matrix[i][jdec] + 1;
            matrix[i][j] = min(d1, min(d2, d3));
            if (idec && jdec && s1[idec] == s2[j - 2] && s1[i - 2] == s2[jdec])
                matrix[i][j] = min(matrix[i][j], matrix[i - 2][j - 2] + 1);
        }
#ifdef USUAL_MODE
    for (unsigned i = 0; i < n; ++i)
     {
        for (unsigned j = 0; j < m; ++j)
            std::cout << matrix[i][j] << ' ';
        std::cout << '\n';
    }
#endif
    return matrix[n - 1][m - 1];
}
