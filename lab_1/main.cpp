#include "building_mode.h"
#ifndef TEST_MODE
#include <iostream>
#include <string>
#include "algorithms.h"

#ifdef TIMING_MODE
unsigned long long tick()
{
    unsigned long long d;
    __asm__ __volatile__ ("rdtsc" : "=A" (d) );
    return d;
}
#endif

int main()
{
    std::string s1, s2;
    std::cout << "Enter string 1\n";
    std::getline(std::cin, s1);
    std::cout << "Enter string 2\n";
    std::getline(std::cin, s2);
    char choice = 0;
    unsigned (*distance)(const std::string&, const std::string&);
    unsigned res = 0;
    while (choice < '1' || choice > '4')
    {
        std::cout << "Choose algorythm:\n1.Recursive Levenstein\n2.Matrix Levenstein\n3.Recursive Damerau-Levenstein\n4.Matrix Damerau-Levenstein\n";
        std::cin >> choice;
        switch (choice)
        {
        case '1':
            distance = recursive_levenstein;
            break;
        case '2':
            distance = matrix_levenstein;
            break;
        case '3':
            distance = recursive_damerau;
            break;
        case '4':
            distance = matrix_damerau;
            break;
        default:
            std::cout << "Wrong input\n";
        };
    }
#ifdef TIMING_MODE
    unsigned long long tb, te;
    tb = tick();
    res = distance(s1, s2);
    te = tick();
    std::cout << "Passed " << te - tb << " ticks\n";
#else
    res = distance(s1, s2);
#endif
    std::cout << "Result: " << res;
    return 0;
}
#endif
