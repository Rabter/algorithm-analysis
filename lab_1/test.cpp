#include "building_mode.h"
#ifdef TEST_MODE
#include <ctime>
#include <string>
#include <fstream>
#include <iostream>
#include "algorithms.h"

inline char generate_letter()
{
    return rand() % ('a' - 'z') + 'a';
}

unsigned long long tick()
{
    unsigned long long d;
    __asm__ __volatile__ ("rdtsc" : "=A" (d) );
    return d;
}

void word_generator(std::string &word1, std::string &word2, unsigned len1, unsigned len2)
{
    unsigned maxlen = (len1 > len2? len1 : len2);
    word1 = "";
    word2 = "";
    for (unsigned i = 0; i < maxlen; ++i)
    {
        if (word1.length() < len1)
            word1.push_back(generate_letter());
        if (word2.length() < len1)
            word2.push_back(generate_letter());
    }
}

int main()
{
    srand(time(NULL));
    std::string s1, s2;
    unsigned long long t1, t2, t3, tl, td;
    std::ofstream fout("time_results.csv");
    fout << "I;Levenstein;Damerau-Levenstein:\n";
    fout << "Recursive algorithms:\n";
    std::cout << "Started timetesting recursive\n";
    for (int i = 0; i < 8; ++i)
    {
        tl = td = 0;
        int repeat_count = 50 / (i + 1);
        for (int j = 0; j < repeat_count; ++j)
        {
            word_generator(s1, s2, i, i);
            t1 = tick();
            recursive_levenstein(s1, s2);
            t2 = tick();
            recursive_damerau(s1, s2);
            t3 = tick();
            tl += t2 - t1;
            td += t3 - t2;
        }
        fout << i << ";" << tl / repeat_count << ";" << td / repeat_count << "\n";
    }
    std::cout << "Finished timetesting recursive\n";
    fout << "Matrix algorithms:\n";
    for (int i = 100; i < 800; i += 100)
    {
        tl = td = 0;
        int repeat_count = 50;
        for (int j = 0; j < repeat_count; ++j)
        {
            word_generator(s1, s2, i, i);
            t1 = tick();
            matrix_levenstein(s1, s2);
            t2 = tick();
            matrix_damerau(s1, s2);
            t3 = tick();
            tl += t2 - t1;
            td += t3 - t2;
        }
        fout << i << ";" << tl / repeat_count << ";" << td / repeat_count << "\n";
    }
    std::cout << "Finished timetesting matrix\n";
}

#endif
