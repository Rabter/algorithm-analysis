#ifndef ALGORITHMS_H
#define ALGORITHMS_H

#include <string>

unsigned recursive_levenstein(const std::string &s1, const std::string &s2);
unsigned recursive_damerau(const std::string &s1, const std::string &s2);
unsigned matrix_levenstein(const std::string &s1, const std::string &s2);
unsigned matrix_damerau(const std::string &s1, const std::string &s2);

#endif // ALGORITHMS_H
