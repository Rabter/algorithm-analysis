#include "matrix.h"

Matrix::Matrix(): n(0), m(0), mat(nullptr) {}

Matrix::Matrix(unsigned height, unsigned width): n(height), m(width)
{
    mat = new int*[n];
    for (unsigned i = 0; i < n; ++i)
        mat[i] = new int[m];
}

Matrix::Matrix(Matrix &&other)
{
    mat = other.mat;
    n = other.n;
    m = other.m;
    other.mat = nullptr;
}

Matrix::~Matrix()
{
    if (mat)
    {
        for (unsigned i = 0; i < n; ++i)
            delete mat[i];
        delete mat;
    }
}

void Matrix::reset_height(unsigned new_height)
{
    int **new_mat = new int*[new_height];
    unsigned minh = (new_height < n? new_height : n), i = 0;
    for (;i < minh; ++i)
        new_mat[i] = mat[i];
    if (new_height < n)
    {
        for (;i < new_height; ++i)
            delete mat[i];
    }
    else
    {
        for (;i < new_height; ++i)
            new_mat[i] = new int[m];
    }
    delete mat;
    mat = new_mat;
    n = new_height;
}

void Matrix::reset_width(unsigned new_width)
{
    for (unsigned i = 0; i < n; ++i)
    {
        int *new_row = new int[new_width];
        unsigned minw = (new_width < m? new_width : m);
        for (unsigned j = 0; j < minw; ++j)
            new_row[j] = mat[i][j];
        delete mat[i];
        mat[i] = new_row;
    }
    m = new_width;
}

void Matrix::reset_matrix(unsigned new_heigth, unsigned new_width)
{
    if (mat)
    {
        for (unsigned i = 0; i < n; ++i)
            delete mat[i];
        delete mat;
    }
    n = new_heigth;
    m = new_width;
    *this = Matrix(new_heigth, new_width);
}

void Matrix::clear()
{
    if (mat)
    {
        for (unsigned i = 0; i < n; ++i)
            delete mat[i];
        delete mat;
        mat = nullptr;
    }
    n = m = 0;
}

bool Matrix::is_valid() const
{
    return mat;
}

Matrix &Matrix::operator =(Matrix &&other)
{
    mat = other.mat;
    n = other.n;
    m = other.m;
    other.mat = nullptr;
    return *this;
}

bool operator ==(const Matrix &a, const Matrix &b)
{
    if (a.height() != b.height() || a.width() != b.width())
        return false;
    for (unsigned i = 0; i < a.height(); ++i)
        for (unsigned j = 0; j < a.width(); ++j)
            if (a[i][j] != b[i][j])
                return false;
    return true;
}

std::ostream& operator <<(std::ostream &stream, const Matrix &mat)
{
    for (unsigned i = 0; i < mat.n; ++i)
    {
        for (unsigned j = 0; j < mat.m; ++j)
            stream << mat[i][j] << ' ';
        stream << '\n';
    }
    return stream;
}
