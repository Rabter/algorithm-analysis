#include "building_mode.h"
#ifdef REGULAR_MODE
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <ctime>
#include "matrix.h"
#include "matrix_math.h"

unsigned long long tick()
{
    unsigned long long d;
    __asm__ __volatile__ ("rdtsc" : "=A" (d) );
    return d;
}

void fill_matrix(Matrix &m)
{
    for (unsigned i = 0; i < m.height(); ++i)
        for (unsigned j = 0; j < m.width(); ++j)
            m[i][j] = rand();
}

int main()
{
    unsigned n, m;
    std::cout << "Enter height and width of the first matrix:\n";
    std::cin >> n >> m;
    Matrix first(n, m);
    std::cout << "Enter height and width of the second matrix:\n";
    std::cin >> n >> m;
    Matrix second(n, m);

    if (first.width() != second.height())
        std::cout << "Wrong input size\n";
    else
    {
        srand(time(NULL));
        int choice;
        std::cout << "Type 1 for manual input or 0 for random values:\n";
        std::cin >> choice;
        if (choice)
        {
            std::cout << "Enter first matrix:\n";
            for (unsigned i = 0; i < first.height(); ++i)
                for (unsigned j = 0; j < first.width(); ++j)
                    std::cin >> first[i][j];
            std::cout << "Enter second matrix:\n";
            for (unsigned i = 0; i < second.height(); ++i)
                for (unsigned j = 0; j < second.width(); ++j)
                    std::cin >> second[i][j];
        }
        else
        {
            fill_matrix(first);
            fill_matrix(second);
        }

        Matrix result_simple, result_vinograd, result_vinograd_opt;
        unsigned long long t1, t2, t3, t4;

        t1 = tick();
        simple_multiplication(result_simple, first, second);
        t2 = tick();
        vinograd_multiplication(result_vinograd, first, second);
        t3 = tick();
        vinograd_optimized_multiplication(result_vinograd_opt, first, second);
        t4 = tick();

        if (result_simple == result_vinograd && result_simple == result_vinograd_opt)
        {
            std::cout << "\nResult:\n" << result_simple;
            std::cout << "\nMultiplication time:\n";
            std::cout << std::setw(10) << std::left << "Simple" << std::setw(10) << std::left << "Vinograd" << "Optimized" << std::endl;
            std::cout << std::setw(10) << std::left << t2 - t1 << std::setw(10) << std::left << t3 - t2 <<  t4 - t3;
        }
        else
            std::cout << "Error. Results mismatch\n";
    }
}
#endif
