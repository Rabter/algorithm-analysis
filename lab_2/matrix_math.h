#ifndef MATRIX_MATH_H
#define MATRIX_MATH_H

#include "matrix.h"

bool simple_multiplication(Matrix &result, const Matrix &first, const Matrix &second);
bool vinograd_multiplication(Matrix &result, const Matrix &first, const Matrix &second);
bool vinograd_optimized_multiplication(Matrix &result, const Matrix &first, const Matrix &second);

#endif // MATRIX_MATH_H
