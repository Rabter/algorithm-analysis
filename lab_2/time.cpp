#include "building_mode.h"
#ifdef TIMING_MODE
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include  <chrono>
#include "matrix.h"
#include "matrix_math.h"

void fill_matrix(Matrix &m)
{
    for (unsigned i = 0; i < m.height(); ++i)
        for (unsigned j = 0; j < m.width(); ++j)
            m[i][j] = rand();
}

int main()
{
    srand(time(NULL));
    std::ofstream fout("time_results.csv");
    fout << "Size;Simple;Vinograd;Optimized Vinograd\n";
    Matrix first, second, result;
    std::chrono::time_point<std::chrono::system_clock> t1, t2, t3, t4;
    for (unsigned i = 100, divider = 1; i <= 1000; i += 100, ++divider)
    {
        std::cout <<"Timetesting on " << i <<'x' << i <<"...\n";
        long double ts = 0, tv = 0, tvo = 0;
        first.reset_height(i);
        first.reset_width(i);
        second.reset_height(i);
        second.reset_width(i);
        fill_matrix(first);
        fill_matrix(second);

        unsigned limit = 10 / divider;
        for (unsigned j = 0; j < limit; ++j)
        {
            t1 = std::chrono::system_clock::now();
            simple_multiplication(result, first, second);
            t2 = std::chrono::system_clock::now();
            vinograd_multiplication(result, first, second);
            t3 = std::chrono::system_clock::now();
            vinograd_optimized_multiplication(result, first, second);
            t4 = std::chrono::system_clock::now();
            ts += static_cast<long double>(std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
            tv += static_cast<long double>(std::chrono::duration_cast<std::chrono::nanoseconds>(t3 - t2).count());
            tvo += static_cast<long double>(std::chrono::duration_cast<std::chrono::nanoseconds>(t4 - t3).count());
        }
        ts /= limit;
        tv /= limit;
        tvo /= limit;
        fout << i << ";" << ts << ";" << tv << ";" << tvo << "\n";
    }
    fout << ";;;\n";
    for (unsigned i = 101, divider = 1; i <= 1001; i += 100, ++divider)
    {
        std::cout <<"Timetesting on " << i <<'x' << i <<"...\n";
        long double ts = 0, tv = 0, tvo = 0;
        first.reset_height(i);
        first.reset_width(i);
        second.reset_height(i);
        second.reset_width(i);
        fill_matrix(first);
        fill_matrix(second);

        unsigned limit = 10 / divider;
        for (unsigned j = 0; j < limit; ++j)
        {
            t1 = std::chrono::system_clock::now();
            simple_multiplication(result, first, second);
            t2 = std::chrono::system_clock::now();
            vinograd_multiplication(result, first, second);
            t3 = std::chrono::system_clock::now();
            vinograd_optimized_multiplication(result, first, second);
            t4 = std::chrono::system_clock::now();
            ts += static_cast<long double>(std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count());
            tv += static_cast<long double>(std::chrono::duration_cast<std::chrono::nanoseconds>(t3 - t2).count());
            tvo += static_cast<long double>(std::chrono::duration_cast<std::chrono::nanoseconds>(t4 - t3).count());
        }
        ts /= limit;
        tv /= limit;
        tvo /= limit;
        fout << i << ";" << ts << ";" << tv << ";" << tvo << "\n";
    }
    std::cout <<"Finished timetesting\n";
}

#endif
