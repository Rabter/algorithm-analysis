TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp \
        matrix.cpp \
        matrix_math.cpp \
        time.cpp

HEADERS += \
    building_mode.h \
    matrix.h \
    matrix_math.h
