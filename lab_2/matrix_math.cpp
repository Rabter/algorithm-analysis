#include "matrix_math.h"

bool simple_multiplication(Matrix &result, const Matrix &first, const Matrix &second)
{
    if (!(first.is_valid() && second.is_valid() && (first.width() == second.height())))
        return false;
    result.reset_matrix(first.height(), second.width());
    for (unsigned i = 0; i < first.height(); ++i)
    {
        for (unsigned j = 0; j < second.width(); ++j)
            result[i][j] = 0;
    }
    for (unsigned i = 0; i < first.height(); ++i)
    {
        for (unsigned j = 0; j < second.width(); ++j)
        {
            for (unsigned k = 0; k < first.width(); ++k)
                result[i][j] += first[i][k] * second[k][j];
        }
    }
    return true;
}

bool vinograd_multiplication(Matrix &result, const Matrix &first, const Matrix &second)
{
    if (!(first.is_valid() && second.is_valid() && (first.width() == second.height())))
        return false;
    unsigned common_size = first.width();
    unsigned resh = first.height(), resw = second.width();
    result.reset_matrix(resh, resw);

    int *row_factor = new int[resh];
    int *column_factor = new int[resw];

    unsigned i = 0, j;
    for (; i < resh; ++i)
    {
      row_factor[i] = 0;
      for (j = 0; j < common_size / 2; ++j)
          row_factor[i] = row_factor[i] + first[i][2 * j + 1] * first[i][2 * j];
    }
    for (i = 0; i < resw; ++i)
    {
        column_factor[i] = 0;
        for (j = 0; j < common_size / 2; ++j)
            column_factor[i] = column_factor[i] + second[2 * j + 1][i] * second[2 * j][i];
    }

    for (i = 0; i < resh; ++i)
    {
        for (j = 0; j < resw; ++j)
        {
            result[i][j] = -row_factor[i] - column_factor[j];
            for (unsigned k = 0; k < common_size / 2; ++k)
                result[i][j] = result[i][j] + (first[i][2 * k] + second[2 * k + 1][j]) * (first[i][2 * k + 1] + second[2 * k][j]);
        }
    }

    if (common_size % 2)
    {
        for (i = 0; i < resh; ++i)
        {
            for (j = 0; j < resw; ++j)
                result[i][j] = result[i][j] + first[i][common_size - 1] * second[common_size - 1][j];
        }
    }
    return true;
}

bool vinograd_optimized_multiplication(Matrix &result, const Matrix &first, const Matrix &second)
{
    if (!(first.is_valid() && second.is_valid() && (first.width() == second.height())))
        return false;
    unsigned common_size = first.width();
    unsigned resh = first.height(), resw = second.width();
    result.reset_matrix(resh, resw);

    int *row_factor = new int[resh];
    int *column_factor = new int[resw];

    unsigned i = 0, j;
    for (; i < resh; ++i)
    {
      row_factor[i] = 0;
      for (j = 1; j < common_size; j += 2)
          row_factor[i] -= first[i][j - 1] * first[i][j];
    }
    for (i = 0; i < resw; ++i)
    {
        column_factor[i] = 0;
        for (j = 1; j < common_size; j += 2)
            column_factor[i] -= second[j - 1][i] * second[j][i];
    }

    for (i = 0; i < resh; ++i)
    {
        for (j = 0; j < resw; ++j)
        {
            result[i][j] = row_factor[i] + column_factor[j];
            for (unsigned k = 1; k < common_size; k += 2)
                result[i][j] += (first[i][k] + second[k - 1][j]) * (first[i][k - 1] + second[k][j]);
        }
    }

    if (common_size % 2)
    {
        for (i = 0; i < resh; ++i)
        {
            for (j = 0; j < resw; ++j)
                result[i][j] += first[i][common_size - 1] * second[common_size - 1][j];
        }
    }
    return true;
}
