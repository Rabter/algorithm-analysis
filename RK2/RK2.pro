TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        algorithms.cpp \
        main.cpp \
        state.cpp \
        symcheck.cpp

HEADERS += \
    algorithms.h \
    state.h \
    symcheck.h
