#include <iostream>
#include <vector>
#include <string>
#include "algorithms.h"

int main()
{
    std::string text;
    std::cout << "Enter text:\n";
    std::getline(std::cin, text);
    std::vector<std::string> res;

    re_election(res, text);
    std::cout << "\n\nRegular expressions result:\n";
    for (std::string token: res)
        std::cout << "'" << token << "' ";

    res.clear();

    fa_election(res, text);
    std::cout << "\n\nFinite automaton result:\n";
    for (std::string token: res)
        std::cout << "'" << token << "' ";
}
