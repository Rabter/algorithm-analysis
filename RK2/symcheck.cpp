#include "symcheck.h"

bool is_punctuation(char sym)
{
    static std::string punc_marks = ".?!,:";
    return punc_marks.find(sym) != std::string::npos;
}

bool is_letter(char sym)
{
    return sym >= 'a' && sym <= 'z' || sym >= 'A' && sym <= 'Z';
}
