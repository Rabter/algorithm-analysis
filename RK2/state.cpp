#include <locale>
#include "state.h"
#include "symcheck.h"

namespace state_types
{

void WaitingForWord::clearing_next(State *&next, char sym)
{
    const char *loc = "C";
    if (std::isspace(sym, std::locale(loc)))
    {
        state = WAITING_FOR_WORD;
        next = this;
    }
    else
    {
        if (is_letter(sym))
            next = new ReadingWord;
        else if (sym == '\0')
            next = new Finished;
        else
            next = new IncorrectSymbol;
        delete this; // Quote from ISO's FAQ: "As long as you’re careful, it’s okay (not evil) for an object to commit suicide (delete this)."
    }
}

void ReadingWord::clearing_next(State *&next, char sym)
{
    if (is_letter(sym))
    {
        next = this;
        state = READING_WORD;
    }
    else
    {
        const char *loc = "C";
        if (std::isspace(sym, std::locale(loc)))
            next = new WaitingForWord(CORRECT_WORD);
        else if (sym == '-')
            next = new GotGyphen;
        else if (sym == '\0')
            next = new Finished(CORRECT_WORD);
        else if (is_punctuation(sym))
            next = new Punctuation;
        else
            next = new IncorrectSymbol;

        delete this;
    }
}

void GotGyphen::clearing_next(State *&next, char sym)
{
    const char *loc = "C";
    if (is_letter(sym))
        next = new ReadingWord(READING_WORD);
    else if (sym == '\0')
        next = new Finished;
    else if (std::isspace(sym, std::locale(loc)))
        next = new WaitingForWord;
    else
        next = new IncorrectSymbol;

    delete this;
}

void Finished::clearing_next(State *&next, char sym)
{
    next = this;
    (void)sym;
}

void IncorrectSymbol::clearing_next(State *&next, char sym)
{
    const char *loc = "C";
    if (std::isspace(sym, std::locale(loc)))
    {
        next = new WaitingForWord();
        delete this;
    }
    else if (sym == '\0')
    {
        next = new Finished();
        delete this;
    }
    else
        next = this;
}

void Punctuation::clearing_next(State *&next, char sym)
{
    const char *loc = "C";
    if (std::isspace(sym, std::locale(loc)))
        next = new WaitingForWord(CORRECT_WORD);
    else if (sym == '\0')
        next = new Finished(CORRECT_WORD);
    else
        next = new IncorrectSymbol;
    delete this;
}

}
