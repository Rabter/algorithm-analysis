#ifndef SYMCHECK_H
#define SYMCHECK_H

#include <string>

bool is_punctuation(char sym);
bool is_letter(char sym);

#endif // SYMCHECK_H
