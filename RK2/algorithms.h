#ifndef ALGORITHMS_H
#define ALGORITHMS_H

#include <vector>
#include <string>

void re_election(std::vector<std::string> &result, const std::string &text);
void fa_election(std::vector<std::string> &result, const std::string &text);

#endif // ALGORITHMS_H
