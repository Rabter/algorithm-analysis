#ifndef STATE_H
#define STATE_H


namespace state_types
{

// Automaton states
class WaitingForWord;
class ReadingWord;
class b;
typedef WaitingForWord initial;
enum states {WAITING_FOR_WORD, STARTED_WORD, READING_WORD, CORRECT_WORD, PUNCTUATION_SIGN, END_OF_TEXT};

}

class State
{
public:
    virtual void clearing_next(State *&next, char sym) = 0;
    inline state_types::states name()
    { return state; }
    virtual ~State() {}
protected:
    state_types::states state;
};

namespace state_types
{

class WaitingForWord: public State
{
public:
    inline WaitingForWord(states state = WAITING_FOR_WORD) { this->state = state; }
    virtual void clearing_next(State *&next, char sym) override;
    virtual ~WaitingForWord() {}

private:
};

class ReadingWord: public State
{
public:
    inline ReadingWord(states state = STARTED_WORD) { this->state = state; }
    virtual void clearing_next(State *&next, char sym) override;
    virtual ~ReadingWord() {}

};

class GotGyphen: public ReadingWord
{
public:
    inline GotGyphen() { this->state = READING_WORD; }
    virtual void clearing_next(State *&next, char sym) override;
    virtual ~GotGyphen() {}
};

class Punctuation: public State
{
public:
    inline Punctuation(states state = PUNCTUATION_SIGN) { this->state = state; }
    virtual void clearing_next(State *&next, char sym) override;
    virtual ~Punctuation() {}
};

class IncorrectSymbol: public State
{
public:
    inline IncorrectSymbol(states state = WAITING_FOR_WORD) { this->state = state; }
    virtual void clearing_next(State *&next, char sym) override;
    virtual ~IncorrectSymbol() {}
};

class Finished: public State
{
public:
    inline Finished(states state = END_OF_TEXT) { this->state = state; }
    virtual void clearing_next(State *&next, char sym) override;
    virtual ~Finished() {}
};

}

#endif // STATE_H
