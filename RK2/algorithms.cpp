#include <regex>
#include "algorithms.h"
#include "state.h"
#include "symcheck.h"

void re_election(std::vector<std::string> &result, const std::string &text)
{
    std::string source(text);
    std::smatch match;
    source += ' '; // c++ compilers can't treat $ as end of string
    std::regex expr("(^|[ \f\n\r\t\v])([a-z]|[A-Z])+(-([a-z]|[A-Z])+)*([.?!,:]?[ \f\n\r\t\v])");


    while (std::regex_search(source, match, expr, std::regex_constants::match_not_eol))
    {
        std::string res = match.str();
        const char *loc = "C";
        if (std::isspace(res[0], std::locale(loc)))
            res.erase(res.begin());
        if (std::isspace(res.back(), std::locale(loc)))
            res.erase(res.end() - 1);
        if (is_punctuation(res.back()))
            res.erase(res.end() - 1);
        result.push_back(res);
        source = " " + match.suffix().str();
    }
}

void fa_election(std::vector<std::string> &result, const std::string &text)
{
    State *state = new state_types::WaitingForWord;
    State *next_state;
    unsigned size = text.size();
    unsigned ib, ie;
    for (unsigned i = 0; i <= size; ++i)
    {
        char sym = text[i];
        state->clearing_next(next_state, sym);
        state = next_state;
        switch (state->name())
        {
        case state_types::STARTED_WORD:
            ib = i;
            ie = 0;
            break;
        case state_types::PUNCTUATION_SIGN:
            ie = i;
            break;
        case state_types::CORRECT_WORD:
            if (!ie)
                ie = i;
            result.push_back(std::string(text.begin() + ib, text.begin() + ie));
            break;
        };
    }
}
