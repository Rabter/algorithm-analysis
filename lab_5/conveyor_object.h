#ifndef CONVEYOROBJECT_H
#define CONVEYOROBJECT_H


class ConveyorObject
{
public:
    inline ConveyorObject(int value = 0): value(value), my_number(total_objects_number()++) {}
    inline unsigned number() const { return my_number; }
    int value;

private:
    inline unsigned& total_objects_number()
    {
        static unsigned count = 0;
        return count;
    }
    const unsigned my_number;
};

#endif // CONVEYOROBJECT_H
