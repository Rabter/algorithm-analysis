#include <chrono>
#include <ctime>
#include <mutex>
#include "conveyor.h"

#ifdef MILLISECONDS
#define NOW (clock())
#define TIME_TO_OUTPUT(t) "After " << (t) << "ms"
typedef clock_t my_time_t;
#else
#define NOW (std::chrono::system_clock::to_time_t(std::chrono::system_clock::now()))
#define TIME_TO_OUTPUT(t) (ctime(&t))
typedef time_t my_time_t;
#endif

std::mutex log_locker;

Conveyor::Conveyor(sleeper func, std::ofstream &log, std::queue<ConveyorObject *> *next):
    my_number(total_conveyors_number()++), delay(func), log(log), next(next) {}

void Conveyor::run()
{
    while (!log.is_open()); // Waiting for logand next to be initialized
    while (!next);

    my_time_t t = NOW;
    log_locker.lock();
    log << TIME_TO_OUTPUT(t) << " Conveyor line " << my_number << " has been turned on" << std::endl << std::endl;
    log_locker.unlock();

    ConveyorObject *object;
    do
    {
        while (!size());
        object = front();
        pop();
        if (object->value > -1)
        {
            t = NOW;
            log_locker.lock();
            log << TIME_TO_OUTPUT(t) << " Conveyor line " << my_number << " recieved object " << object->number() << std::endl << std::endl;
            log_locker.unlock();
            delay(object->value);
            t = NOW;
            log_locker.lock();
            log << TIME_TO_OUTPUT(t) << " Conveyor line " << my_number << " finished processing object " << object->number() << std::endl << std::endl;
            log_locker.unlock();
        }
        else
        {
            t = NOW;
            log_locker.lock();
            log << TIME_TO_OUTPUT(t) << " Conveyor line " << my_number << " has been turned off" << std::endl << std::endl;
            log_locker.unlock();
        }
        next->push(object);
    } while (object->value > -1);
}
