#include <iostream>
#include <fstream>
#include <thread>
#include <chrono>
#include <cstdlib>
#include <ctime>
#include "conveyor.h"
#define N 10

void sleep_300ms(int a)
{
#if __cplusplus == 201103
    std::this_thread::sleep_for(std::chrono::milliseconds(300));
#else
    using std::chrono_literals::operator ""ms;  // Added as a reminder. Qt may argue as it doesn't like c++14, but it compiles
    std::this_thread::sleep_for(300ms);
#endif
    (void)a;
}

void sleep_for(int a)
{
    std::this_thread::sleep_for(std::chrono::milliseconds(a));
}

void sleep_rand(int a)
{
    std::this_thread::sleep_for(std::chrono::milliseconds(rand() % 500 + 100));
    (void)a;
}

void sleep_rand_by(int a)
{
    std::this_thread::sleep_for(std::chrono::milliseconds(rand() % a + 200));
}

int main()
{
    srand(time(NULL));
    std::ofstream log("log.txt");
    std::queue<ConveyorObject*> result;
    Conveyor line0(sleep_for, log);
    Conveyor line1(sleep_300ms, log);
    Conveyor line2(sleep_rand, log);
    Conveyor line3(sleep_rand_by, log);
    Conveyor line4(sleep_for, log);

    line0.set_next(&line1);
    line1.set_next(&line2);
    line2.set_next(&line3);
    line3.set_next(&line4);
    line4.set_next(&result);

    std::thread runner0{Conveyor::run, std::ref(line0)};
    std::thread runner1{Conveyor::run, std::ref(line1)};
    std::thread runner2{Conveyor::run, std::ref(line2)};
    std::thread runner3{Conveyor::run, std::ref(line3)};
    std::thread runner4{Conveyor::run, std::ref(line4)};

    ConveyorObject objects[N];
    for (unsigned i = 0; i < N - 1; ++i)
        objects[i].value = rand() % 500 + 100;
    objects[N - 1].value = -1;

    for (unsigned i = 0; i < N; ++i)
        line0.push(objects + i);

    runner0.join();
    runner1.join();
    runner2.join();
    runner3.join();
    runner4.join();

    log.close();

    if (result.size() == N)
        std::cout << "All objects have been processed sucsessfully\n";
    else
        std::cout << "Result queue has incorrect size\n";
}
