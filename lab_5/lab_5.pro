TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        conveyor.cpp \
        main.cpp

HEADERS += \
    conveyor.h \
    conveyor_object.h
