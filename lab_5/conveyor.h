#ifndef CONVEYOR_H
#define CONVEYOR_H

#include <queue>
#include <fstream>
#include "conveyor_object.h"

typedef void (*sleeper)(int);

class Conveyor: public std::queue<ConveyorObject*>
{
public:
    Conveyor(sleeper func, std::ofstream &log, std::queue<ConveyorObject*> *next = nullptr);
    inline void set_next(std::queue<ConveyorObject*> *next) { this->next = next; }
    void run();
private:
    inline unsigned& total_conveyors_number()
    {
        static unsigned count = 0;
        return count;
    }
    const unsigned my_number;
    sleeper delay;
    std::ofstream &log;
    std::queue<ConveyorObject*> *next;
};

#endif // CONVEYOR_H
