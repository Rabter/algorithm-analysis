#include "building_mode.h"
#ifdef TIMING_MODE

#include <fstream>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include "sorters.h"
#define MAX_SIZE 1001
#define SIZE_STEP 100

unsigned long long tick()
{
    unsigned long long d;
    __asm__ __volatile__ ("rdtsc" : "=A" (d) );
    return d;
}

int main()
{
    srand(time(NULL));
    std::ofstream fout("time_results.csv");
    int arr1[MAX_SIZE], arr2[MAX_SIZE], arr3[MAX_SIZE];
    unsigned long long t1, t2, t3, t4, tb = 0, ts = 0, tq = 0;
    fout << "Size;Bubble;Selection;Quick\n";

    fout << "Sorted\n";
    for (int size = SIZE_STEP; size <= MAX_SIZE - 1; size += SIZE_STEP)
    {
        tb = 0, ts = 0, tq = 0;
        for (int i = 0; i < 50; ++i)
        {
            for (int j = 0; j < size; ++j)
                arr1[j] = arr2[j] = arr3[j] = j;

            t1 = tick();
            bubble_sort(arr1, size);
            t2 = tick();
            selection_sort(arr2, size);
            t3 = tick();
            quick_sort(arr3, size);
            t4 = tick();
            if (t4 > t3 && t3 > t2 && t2 > t1)
            {
                tb += t2 - t1;
                ts += t3 - t2;
                tq += t4 - t3;
            }
            else
            {
                --i; // tick() goes wierd sometimes
                std::cout << "Achtung " << i << "\n";
            }
        }
        fout << size << ';' << tb / 50 << ';' << ts / 50 << ';' << tq / 50 << '\n';
    }

    fout << "Sorted backwards\n";
    for (int size = SIZE_STEP; size <= MAX_SIZE - 1; size += SIZE_STEP)
    {
        tb = 0, ts = 0, tq = 0;
        for (int i = 0; i < 50; ++i)
        {
            for (int j = 0; j < size; ++j)
                arr1[j] = arr2[j] = arr3[j] = size - j;

            t1 = tick();
            bubble_sort(arr1, size);
            t2 = tick();
            selection_sort(arr2, size);
            t3 = tick();
            quick_sort(arr3, size);
            t4 = tick();
            if (t4 > t3 && t3 > t2 && t2 > t1)
            {
                tb += t2 - t1;
                ts += t3 - t2;
                tq += t4 - t3;
            }
            else
            {
                --i; // tick() goes wierd sometimes
                std::cout << "Achtung " << i << "\n";
            }
        }
        fout << size << ';' << tb / 50 << ';' << ts / 50 << ';' << tq / 50 << '\n';
    }

    fout << "Random\n";
    for (int size = SIZE_STEP; size <= MAX_SIZE - 1; size += SIZE_STEP)
    {
        tb = 0, ts = 0, tq = 0;
        for (int i = 0; i < 50; ++i)
        {
            for (int j = 0; j < size; ++j)
                arr1[j] = arr2[j] = arr3[j] = rand();

            t1 = tick();
            bubble_sort(arr1, size);
            t2 = tick();
            selection_sort(arr2, size);
            t3 = tick();
            quick_sort(arr3, size);
            t4 = tick();
            if (t4 > t3 && t3 > t2 && t2 > t1)
            {
                tb += t2 - t1;
                ts += t3 - t2;
                tq += t4 - t3;
            }
            else
            {
                --i; // tick() goes wierd sometimes
                std::cout << "Achtung " << i << "\n";
            }
        }
        fout << size << ';' << tb / 50 << ';' << ts / 50 << ';' << tq / 50 << '\n';
    }
    std::cout << "Done timing\n";
}

#endif
