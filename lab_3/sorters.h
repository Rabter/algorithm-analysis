#ifndef SORTERS_H
#define SORTERS_H

typedef unsigned long long size_type;

void selection_sort(int *arr, size_type n);
void bubble_sort(int *arr, size_type n);
void quick_sort(int *arr, size_type n);

#endif // SORTERS_H
