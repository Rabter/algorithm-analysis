#include "building_mode.h"
#ifdef REGULAR_MODE

#include <iostream>
#include <iomanip>
#include "sorters.h"
#define MAX_SIZE 20

unsigned long long tick()
{
    unsigned long long d;
    __asm__ __volatile__ ("rdtsc" : "=A" (d) );
    return d;
}

int main()
{
    int n;
    int arr1[MAX_SIZE], arr2[MAX_SIZE], arr3[MAX_SIZE];

    std::cout << "Enter array size:\n";
    std::cin >> n;

    if (n > MAX_SIZE)
    {
        std::cout << "Too big size\n";
        return 1;
    }
    if (n < 1)
    {
        std::cout << "Size has to be a positive integer\n";
        return 1;
    }

    std::cout << "Enter " << n << " element(s) of array:\n";
    for (int i = 0; i < n; ++i)
    {
        int num;
        std::cin >> num;
        arr1[i] = num;
        arr2[i] = num;
        arr3[i] = num;
    }

    unsigned long long t1, t2, t3, t4, ts, tb, tq;
    t1 = tick();
    bubble_sort(arr1, n);
    t2 = tick();
    selection_sort(arr2, n);
    t3 = tick();
    quick_sort(arr3, n);
    t4 = tick();
    tb = t2 - t1;
    ts = t3 - t2;
    tq = t4 - t3;


    std::cout << "\nResult:";
    std::cout << "\nBubble sort:    ";
    for (int i = 0; i < n; ++i)
        std::cout << arr1[i] << ' ';
    std::cout << "\nSelection sort: ";
    for (int i = 0; i < n; ++i)
        std::cout << arr2[i] << ' ';
    std::cout << "\nQuick sort:     ";
    for (int i = 0; i < n; ++i)
        std::cout << arr3[i] << ' ';

    std::cout << "\n\nTime results:\n";
    std::cout << "Bubble sort:    " << tb << " ticks\n";
    std::cout << "Selection sort: " << ts << " ticks\n";
    std::cout << "Quick sort:     " << tq << " ticks\n";

    return 0;
}

#endif
