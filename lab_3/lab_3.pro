TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp \
        sorters.cpp \
        time.cpp

HEADERS += \
    building_mode.h \
    sorters.h
