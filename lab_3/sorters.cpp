#include "sorters.h"

void swap(int &a, int &b)
{
    int tmp = a;
    a = b;
    b = tmp;
}

void selection_sort(int *arr, size_type n)
{
    size_type ndec = n - 1;
    for (size_type i = 0; i < ndec; i++)
    {
        size_type imin = i;
        for (size_type j = i + 1; j < n; j++)
        {
            if (arr[j] < arr[imin])
                imin = j;
        }
        swap(arr[i], arr[imin]);
        imin = i;
    }
}

void bubble_sort(int *arr, size_type n)
{
    size_type i = 0;
    bool swaped = false;

    while (i < n)
    {
        if (i + 1 != n && arr[i] > arr[i + 1])
        {
            swap(arr[i], arr[i + 1]);
            swaped = true;
        }

        ++i;

        if (i == n && swaped)
        {
            swaped = false;
            i = 0;
        }
    }
}

void quick_sort(int *arr, size_type n)
{
    int i = 0;
    int j = n - 1;
    int mid = arr[n / 2];

    do
    {
        while(arr[i] < mid)
            i++;
        while(arr[j] > mid)
            j--;

        if (i <= j)
        {
            swap(arr[i], arr[j]);
            ++i;
            --j;
        }

    } while (i <= j);

    if (j > 0)
        quick_sort(arr, j + 1);
    if (n - i > 1)
        quick_sort(arr + i, n - i);
}
